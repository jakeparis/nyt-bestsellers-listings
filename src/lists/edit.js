
import apiFetch from '@wordpress/api-fetch';

import {
	SelectControl,
	Placeholder,
	ToggleControl,
	PanelBody,
	PanelRow,
	Spinner,
} from '@wordpress/components';

import { useState, useEffect, useMemo } from '@wordpress/element';

import {
	useBlockProps,
	InspectorControls
} from '@wordpress/block-editor';

import iconNews from './icon.svg.js';

export default function edit( props ) {

	const blockProps = useBlockProps({
		className: "nyt-bestseller-listings-booklist",
	});

	const { attributes, isSelected, setAttributes } = props;

	const [ allLists, setAllLists ] = useState( null );
	
	useEffect( () => {
		const url = ajaxurl + '?action=nyt_bestseller_listings_getAllLists'
		fetch( url ).then( resp => resp.json().then( resp => {
			setAllLists( resp.data );
		}))
	}, [] );

	const allListsSelect = useMemo( () => {
		if( allLists === null )
			return ( <Spinner /> );
		if( allLists.length ) {
			return (
				<SelectControl
					label="Bestseller List"
					value={ attributes.initialList }
					options={ allLists }
					onChange={ initialList => setAttributes({initialList}) }
				/>
			);
		}
		return (
			<p style={{color:'red'}}>Error: Unable to fetch lists</p>
		)
	}, [ allLists, attributes.initialList ] );

	return (
		<div {...blockProps}>
			<Placeholder
				icon={ iconNews }
				label="Bestseller Lists from New York Times"
				instructions="Choose which list displays initially"
			>
				{ allListsSelect }
			</Placeholder>

			<InspectorControls>
				<PanelBody
					title="Settings"
				>
					<PanelRow>
						<ToggleControl
							label="Display Book Cover Images"
							checked={ attributes.displayImages }
							onChange={ displayImages=>setAttributes({displayImages}) }
						/>
					</PanelRow>
				</PanelBody>
			</InspectorControls>

		</div>
	);
}

