import './public.scss';


document.addEventListener("DOMContentLoaded", function() {

	var getListHtml = function(listSlug){
		return new Promise(function(resolve, reject){
			
			const displayImages = ( window.bslnyt && window.bslnyt.displayImages===false)
				? 0 : 1;

			jQuery.get(
				nyt_bestseller_listings_settings.ajaxurl,
				{
					action: 'nyt_bestseller_listings_getList',
					listName: listSlug,
					displayImages,
				},
				function(resp){
					resolve( resp );
				}
			);
		});
	};

	var listSwitchers = document.querySelectorAll('.js-nyt-bestseller-listings_changeList');
	for(var i=0; i<listSwitchers.length; i++){
		listSwitchers[i].addEventListener('change',function(){
			var list = this.parentNode.parentNode.querySelector('.nyt-bestseller-listings-booklist');
			list.classList.add('loading');

			var params = new URLSearchParams(window.location.search);
			params.set('nytlist', this.value );

			history.pushState({},'', '?'+params.toString() );

			getListHtml( this.value ).then(function(html){
				list.innerHTML = html;
				list.classList.remove('loading');
			});

			for(var n=0;n<listSwitchers.length;n++){
				if( listSwitchers[n] === this )
					continue;
				listSwitchers[n].value = this.value;
			}
		});
	}
});